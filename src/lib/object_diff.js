import { isEqual, functions, omit } from 'lodash'

const VALUE_CREATED = 'created'
const VALUE_UPDATED = 'updated'
const VALUE_DELETED = 'deleted'
const VALUE_UNCHANGED = 'unchanged'
const KEYS = [VALUE_CREATED, VALUE_UPDATED, VALUE_DELETED, VALUE_UNCHANGED]

const isDate = function(x) {
  return Object.prototype.toString.call(x) === '[object Date]'
}

const compareValues = (value1, value2) => {
  if (
    isEqual(omit(value1, functions(value1)), omit(value2, functions(value2)))
  ) {
    return VALUE_UNCHANGED
  }
  if (
    isDate(value1) &&
    isDate(value2) &&
    value1.getTime() === value2.getTime()
  ) {
    return VALUE_UNCHANGED
  }
  if (value1 === undefined) {
    return VALUE_CREATED
  }
  if (value2 === undefined) {
    return VALUE_DELETED
  }
  return VALUE_UPDATED
}

const diffFunc = (prevObj, newObj) => {
  return {
    type: compareValues(prevObj, newObj),
    data: newObj === undefined ? prevObj : newObj
  }
}

const objectDiff = (prevObj, newObj) => {
  const diff = KEYS.reduce((accumulator, key) => {
    accumulator[key] = {}
    return accumulator
  }, {})
  for (const key in prevObj || {}) {
    let value2 = undefined
    if (newObj[key] !== undefined) {
      value2 = newObj[key]
    }
    const { type, data } = diffFunc(prevObj[key], value2)
    diff[type][key] = data
  }
  for (const key in newObj || {}) {
    if (key in (prevObj || {})) {
      continue
    }
    const { type, data } = diffFunc(undefined, newObj[key])
    diff[type][key] = data
  }

  return diff
}

export default objectDiff
