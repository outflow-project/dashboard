import { gql, useSubscription } from '@apollo/client'

const RUN_SUBSCRIPTION = gql`
  subscription GetRun($runId: Int!) {
    run_by_pk(id: $runId) {
      tasks {
        id
        name
        input_values
        hostname
        edges {
          downstream_task_id
          upstream_task_id
        }
        state
        runtime_exceptions {
          id
          traceback
          exception_type
          exception_msg
          time
        }
        workflow {
          id
          manager_task_id
          parent_workflow_id
        }
      }
      id
    }
  }
`

function taskStyleFunc(task) {
  let color
  switch (task.state) {
    case 'success':
      color = '#4caf50'
      break
    case 'failed':
      color = '#f44336'
      break
    case 'pending':
      color = '#ff9800'
      break
    case 'running':
      color = '#2196f3'
      break
    default:
      color = '#9e9e9e'
      break
  }

  task.backgroundColor = color

  return task
}

function parseRuns({ run_by_pk }) {
  const result = {
    nodes: {},
    edges: {}
  }

  for (const { edges: taskEdges, id, ...task } of run_by_pk.tasks) {
    // force node id to be a string to avoid indexing issues in the graph
    const node = {
      ...task,
      id: `${id}`,
      type: 'node',
      parent: task.workflow?.manager_task_id
        ? `${task.workflow.manager_task_id}`
        : null
    }

    result.nodes[node.id] = taskStyleFunc(node)
    for (const edge of taskEdges) {
      const edgeId = `${edge.downstream_task_id}-${edge.upstream_task_id}`
      result.edges[edgeId] = {
        type: 'edge',
        id: edgeId,
        target: `${edge.downstream_task_id}`,
        name: null,
        source: `${edge.upstream_task_id}`
      }
    }
  }
  return result
}

export default function useRunSubscription(runId) {
  const { data, ...rest } = useSubscription(RUN_SUBSCRIPTION, {
    variables: { runId }
  })

  let parsedData = undefined
  if (data) {
    parsedData = parseRuns(data)
  }

  return { data: parsedData, ...rest }
}
