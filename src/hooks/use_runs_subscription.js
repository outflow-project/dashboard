import { gql, useSubscription } from '@apollo/client'

const RUNS_SUBSCRIPTION = gql`
  subscription GetRuns {
    run {
      state
      start_time
      uuid
      id
      end_time
    }
  }
`

export default function useRunsSubscription() {
  return useSubscription(RUNS_SUBSCRIPTION, {
    variables: {}
  })
}
