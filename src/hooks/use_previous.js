import React from 'react'

// keep a reference to the previous prop value
export default function usePrevious(value) {
  const ref = React.useRef()
  React.useEffect(() => {
    ref.current = value
  })
  return ref.current
}
