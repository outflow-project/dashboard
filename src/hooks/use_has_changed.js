import usePrevious from './use_previous'

const useHasChanged = value => {
  const previousValue = usePrevious(value)
  return previousValue !== value
}

export default useHasChanged
