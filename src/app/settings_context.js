import { createContext, useContext } from 'react'

export const LOCAL_STORAGE_KEY = 'outflowDashboardSettings'

export const defaultSettings = {
  layoutEngine: 'dagre',
  gqlUri: 'localhost:8080/v1/graphql',
  secureUri: false
}

export const initialSettings =
  {
    ...defaultSettings,
    ...JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY))
  } || defaultSettings

export const SettingsContext = createContext({
  ...defaultSettings,
  updateSettings: () => {}
})

export const useSettingsContext = () => useContext(SettingsContext)
