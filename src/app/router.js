import {
  BrowserRouter,
  Switch,
  Route,
  useLocation,
  Redirect
} from 'react-router-dom'

import Runs from '#/pages/runs'
import Run from '#/pages/runs/[id].js'

function NoMatch() {
  let location = useLocation()

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  )
}

export default function Router() {
  let basename
  if (process.env.PUBLIC_URL) {
    const splittedStr = process.env.PUBLIC_URL.split('/')
    basename = '/' + splittedStr[splittedStr.length - 1]
  }
  let routes = (
    <Switch>
      <Route
        exact
        path="/"
        render={({ location }) => (
          <Redirect
            to={{
              pathname: '/runs/',
              state: { from: location }
            }}
          />
        )}
      />
      <Route exact path="/runs/">
        <Runs />
      </Route>
      <Route exact path="/runs/:id">
        <Run />
      </Route>
      <Route path="*">
        <NoMatch />
      </Route>
    </Switch>
  )

  return <BrowserRouter basename={basename}>{routes}</BrowserRouter>
}
