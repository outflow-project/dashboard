import theme from './theme'
import Router from './router'
import React from 'react'

import createClient from './gql_client'
import { ApolloProvider } from '@apollo/client'
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import {
  SettingsContext,
  initialSettings,
  LOCAL_STORAGE_KEY
} from './settings_context'

function App() {
  const [settings, updateSettingsState] = React.useState(initialSettings)

  const updateSettings = value => {
    updateSettingsState(value)
    console.log(value)
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(value))
    // force page refresh on settings update to trigger:
    // - graph re-render
    // - API reconnection
    window.location.reload(false)
  }

  const client = createClient(settings)

  return (
    <SettingsContext.Provider value={{ ...settings, updateSettings }}>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <ApolloProvider client={client}>
          <Container maxWidth="lg">
            <Router />
          </Container>
        </ApolloProvider>
      </ThemeProvider>
    </SettingsContext.Provider>
  )
}

export default App
