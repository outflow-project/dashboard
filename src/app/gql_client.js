import { split, HttpLink } from '@apollo/client'
import { ApolloClient, InMemoryCache } from '@apollo/client'
import { getMainDefinition } from '@apollo/client/utilities'

import { WebSocketLink } from '@apollo/client/link/ws'

function createClient(settings) {
  const secureUri = settings.secureUri ? 's' : ''
  const wsLink = new WebSocketLink({
    uri: `ws${secureUri}://${settings.gqlUri}`,
    options: {
      reconnect: true
    }
  })

  const httpLink = new HttpLink({
    uri: `http${secureUri}://${settings.gqlUri}`
  })

  const splitLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query)
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      )
    },
    wsLink,
    httpLink
  )

  const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache()
  })

  return client
}

export default createClient
