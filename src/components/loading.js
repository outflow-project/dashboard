import './loading.css'
import React from 'react'
import Typography from '@material-ui/core/Typography'

export default function Loading() {
  return (
    <React.Fragment>
      <Typography
        variant="h5"
        gutterBottom
        align="center"
        style={{ marginTop: '50px' }}
      >
        Loading...
      </Typography>
      <div className="loading-wrap">
        <div className="loading-drop-outer">
          <svg
            className="loading-drop"
            viewBox="0 0 40 40"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect width="40" height="40" />
          </svg>
        </div>
        <div className="loading-ripple loading-ripple-1">
          <svg
            className="loading-ripple-svg"
            viewBox="0 0 60 60"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect width="60" height="60" />
          </svg>
        </div>
        <div className="loading-ripple loading-ripple-2">
          <svg
            className="loading-ripple-svg"
            viewBox="0 0 60 60"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect width="60" height="60" />
          </svg>
        </div>
        <div className="loading-ripple loading-ripple-3">
          <svg
            className="loading-ripple-svg"
            viewBox="0 0 60 60"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            <rect width="60" height="60" />
          </svg>
        </div>
      </div>
    </React.Fragment>
  )
}
