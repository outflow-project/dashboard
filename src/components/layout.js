import React from 'react'
import { Paper, Grid, Box } from '@material-ui/core'
import RouterBreadcrumbs from '#/components/router_breadcrumbs'
import SettingsFormDialog from '#/components/settings_form_dialog'
import OutflowLogo from '#/images/square_logo.svg'
import { makeStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(1)
  }
}))

export default function Layout({ children }) {
  const classes = useStyles()
  return (
    <Grid container spacing={1}>
      <Grid item xs={12}>
        <Paper className={classes.paper}>
          <Box display="flex" alignItems="center">
            <Box minWidth={32}>
              <Link to="/">
                <img src={OutflowLogo} alt="OutflowLogo" width="24px" />
              </Link>
            </Box>
            <Box width="100%" mr={1}>
              <RouterBreadcrumbs />
            </Box>
            <Box minWidth={20}>
              <SettingsFormDialog />
            </Box>
          </Box>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        {children}
      </Grid>
    </Grid>
  )
}
