import React from 'react'
import Loading from './loading'

export default function Async({ Component, hook, hookParams }) {
  const { data, loading, error } = hook(...(hookParams || []))
  if (loading) {
    // return 'Loading...'
    return <Loading />
  } else if (error) {
    return `Error! ${error.message}`
  } else {
    return <Component data={data} />
  }
}
