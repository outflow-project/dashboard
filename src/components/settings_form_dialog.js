import React from 'react'
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Tooltip,
  Box,
  Select,
  MenuItem,
  InputLabel,
  FormControl
} from '@material-ui/core'
import SettingsIcon from '@material-ui/icons/Settings'
import DeleteIcon from '@material-ui/icons/Delete'
import {
  defaultSettings,
  LOCAL_STORAGE_KEY,
  useSettingsContext
} from '#/app/settings_context'

export default function SettingsFormDialog() {
  const [open, setOpen] = React.useState(false)

  const context = useSettingsContext()

  const [settings, setSettings] = React.useState(context)

  React.useEffect(() => {
    setSettings(context)
  }, [context])

  const handleFieldChange = event => {
    const newSettings = { ...settings }
    newSettings[event.target.name] = event.target.value
    setSettings(newSettings)
  }

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleUpdate = () => {
    context.updateSettings(settings)
    setOpen(false)
  }

  const clearLocalStorage = () => {
    localStorage.removeItem(LOCAL_STORAGE_KEY)
    context.updateSettings(defaultSettings)
    setOpen(false)
  }

  return (
    <div>
      <IconButton aria-label="settings" size="small" onClick={handleClickOpen}>
        <SettingsIcon fontSize="inherit" />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="settings-dialog-title"
      >
        <DialogTitle id="settings-dialog-title">
          <Box display="flex" alignItems="center">
            <Box width="100%" mr={1}>
              Settings
            </Box>
            <Box minWidth={20}>
              <Tooltip title="Clear Local storage and reset settings ?">
                <IconButton variant="outlined" onClick={clearLocalStorage}>
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </Box>
          </Box>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please enter your GraphQL (Hasura) URI and select a graph layout.
          </DialogContentText>
          <FormControl fullWidth>
            <InputLabel>Secure URI ?</InputLabel>
            <Select
              labelId="secure-uri"
              id="secure-uri"
              name="secureUri"
              value={settings.secureUri}
              onChange={handleFieldChange}
            >
              <MenuItem value={false}>ws:// (or http://)</MenuItem>
              <MenuItem value={true}>wss:// (or https://)</MenuItem>
            </Select>
          </FormControl>
          <TextField
            margin="dense"
            id="gql_uri"
            name="gqlUri"
            label="GQL URI"
            fullWidth
            value={settings.gqlUri}
            onChange={handleFieldChange}
          />
          <FormControl fullWidth>
            <InputLabel>Graph Layout</InputLabel>
            <Select
              labelId="layout-engine"
              id="layout-engine"
              name="layoutEngine"
              value={settings.layoutEngine}
              label="Graph Layout"
              onChange={handleFieldChange}
            >
              <MenuItem value={'cola'}>Force-directed (cola)</MenuItem>
              <MenuItem value={'dagre'}>Hierarchical (dagre)</MenuItem>
            </Select>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button onClick={handleUpdate} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
