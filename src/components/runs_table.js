import React from 'react'
import { DataGrid } from '@mui/x-data-grid'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button'
import RunStatus from './run_status'
import { isEqual } from 'lodash'

const defaultSortModel = [
  {
    field: 'start_time',
    sort: 'desc'
  }
]

export default function RunsTable({ data }) {
  const [sortModel, setSortModel] = React.useState(defaultSortModel)

  const handleSortModelChange = value => {
    if (!isEqual(value, sortModel)) {
      setSortModel(value)
    }
  }
  return (
    <div
      style={{ height: 'calc(100vh - 100px)', width: '100%', marginBottom: 16 }}
    >
      <DataGrid
        sortModel={sortModel}
        onSortModelChange={handleSortModelChange}
        columns={[
          {
            field: 'run',
            flex: 0.5,
            renderCell: params => {
              return (
                <Button
                  variant="outlined"
                  size="small"
                  component={Link}
                  to={`/runs/${params.row.id}`}
                >
                  {params.row.id}
                </Button>
              )
            }
          },
          {
            field: 'state',
            flex: 0.5,
            renderCell: ({ row }) => <RunStatus run={row} />
          },
          { field: 'start_time', flex: 1 },
          { field: 'end_time', flex: 1 },
          { field: 'uuid', flex: 1 }
        ]}
        rows={data.run}
      />
    </div>
  )
}
