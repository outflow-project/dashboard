import React from 'react'
import Alert from '@material-ui/lab/Alert'
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline'
import CachedIcon from '@material-ui/icons/Cached'

const iconMapping = {
  error: <ErrorOutlineIcon fontSize="inherit" />,
  success: <CheckCircleOutlineIcon fontSize="inherit" />,
  info: <CachedIcon fontSize="inherit" />
}

const stateMapping = {
  failed: 'error',
  success: 'success',
  running: 'info'
}

export default function RunStatus({ run }) {
  return (
    <Alert
      iconMapping={iconMapping}
      severity={stateMapping[run.state]}
      style={{ width: '100%' }}
    >
      {run.state}
    </Alert>
  )
}
