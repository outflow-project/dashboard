import React, { Fragment } from 'react'
import { Paper, LinearProgress, Box, Typography } from '@material-ui/core'
import Diagram from './diagram'
import usePrevious from '#/hooks/use_previous'
import { makeStyles } from '@material-ui/core/styles'
import objectDiff from '#/lib/object_diff'
import { useSettingsContext } from '#/app/settings_context'

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(1)
  }
}))

function computeDiagramElements(prevEdges, edges, prevNodes, nodes) {
  const edgesDiff = objectDiff(prevEdges, edges)
  const nodesDiff = objectDiff(prevNodes, nodes)
  const newElements = {
    created: {},
    updated: {},
    deleted: {}
  }

  for (const key in newElements) {
    newElements[key] = [
      ...Object.values(nodesDiff[key]),
      ...Object.values(edgesDiff[key])
    ].map(elt => ({
      data: elt
    }))
  }

  return newElements
}

function LinearProgressWithLabel(props) {
  return (
    <Box display="flex" alignItems="center">
      <Box width="100%" mr={1}>
        <LinearProgress variant="determinate" {...props} color="secondary" />
      </Box>
      <Box minWidth={35}>
        <Typography variant="body2" color="textSecondary">{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  )
}

function RunDiagram({ data: { nodes, edges } }) {
  const classes = useStyles()
  const prevNodes = usePrevious(nodes)
  const prevEdges = usePrevious(edges)

  const context = useSettingsContext()

  const elements = computeDiagramElements(prevEdges, edges, prevNodes, nodes)

  const tasks = Object.values(nodes)

  const successfulTasks = tasks.filter(task => task.state === 'success')
  const progress =
    (successfulTasks.length * 100) /
    tasks.filter(task => task.state !== 'skipped').length

  return (
    <Fragment>
      <Paper className={classes.paper}>
        <LinearProgressWithLabel variant="determinate" value={progress} />
      </Paper>
      <Paper
        style={{
          height: 'calc(100vh - 190px)',
          width: '100%'
        }}
      >
        <Diagram elements={elements} layoutEngine={context.layoutEngine} />
      </Paper>
    </Fragment>
  )
}

export default RunDiagram
