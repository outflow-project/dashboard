import React from 'react'
import cytoscape from 'cytoscape'
import cola from 'cytoscape-cola'
import dagre from 'cytoscape-dagre'

import {
  IconButton,
  Tooltip,
  Dialog,
  DialogTitle,
  DialogContent
} from '@material-ui/core'
import TransformIcon from '@material-ui/icons/Transform'
import useHasChanged from '#/hooks/use_has_changed'

const graphLayoutEngines = {
  dagre: dagre,
  cola: cola
}

const addListeners = (graph, handleDialogOpen) => {
  let tappedBefore
  let tappedTimeout

  graph.on('tap', 'node', function(event) {
    let tappedNow = event.target
    if (tappedTimeout && tappedBefore) {
      clearTimeout(tappedTimeout)
    }
    if (tappedBefore === tappedNow) {
      tappedNow.trigger('doubleTap')
      tappedBefore = null
    } else {
      tappedTimeout = setTimeout(function() {
        tappedBefore = null
        tappedNow.trigger('singleTap')
      }, 300)
      tappedBefore = tappedNow
    }
  })
  graph.on('doubleTap', 'node', function(event) {
    const evtTarget = event.target
    handleDialogOpen(evtTarget.data())
  })
  graph.on('singleTap', ':parent', function(event) {})
}

const style = [
  {
    selector: 'node',
    style: {
      content: 'data(name)',
      'background-color': 'data(backgroundColor)'
    }
  },
  {
    selector: 'edge',
    style: {
      'curve-style': 'bezier',
      'target-arrow-shape': 'triangle'
    }
  },
  {
    selector: ':parent',
    style: {
      'text-valign': 'top',
      'text-halign': 'center',
      'background-color': '#555',
      'background-opacity': 0.1,
      'border-color': 'data(backgroundColor)',
      'border-width': 2
    }
  }
]

const Diagram = ({ elements, layoutEngine }) => {
  const container = React.useRef(null)
  const [graph, setGraph] = React.useState(null)
  const [layout, setLayout] = React.useState(null)
  const graphIsReady = graph !== null

  const [open, setOpen] = React.useState(false)
  const [dialogData, setDialogData] = React.useState({})
  const [elementsQueue, setElementsQueue] = React.useState([])
  const elementsChanged = useHasChanged(elements)

  const addElements = React.useCallback(
    newElements => setElementsQueue([...elementsQueue, newElements]),
    [elementsQueue]
  )

  const popElements = React.useCallback(() => {
    const [elt, ...rest] = elementsQueue
    if (elt) {
      // if no more elements, no need to reset the queue
      setElementsQueue(rest)
    }
    return elt
  }, [elementsQueue])

  const drawElements = (graph, elements) => {
    const updateLayout = elements.created.length !== 0
    if (updateLayout) {
      graph.add(elements.created)
    }

    elements.updated.forEach(eltData => {
      const elt = graph.getElementById(`${eltData.data.id}`)
      elt.data(eltData.data)
    })

    if (updateLayout) {
      const newLayout = graph.elements().makeLayout({
        nodeDimensionsIncludeLabels: true,
        name: layoutEngine,
        avoidOverlap: true,
        nodeSpacing: node => 30,
        flow: { axis: 'y', minSeparation: 30 }
      })
      setLayout(newLayout)
      newLayout.run()
    }
  }

  const handleDialogOpen = node => {
    setDialogData(node)
    setOpen(true)
  }

  const handleDialogClose = () => {
    setDialogData({})
    setOpen(false)
  }

  /* eslint-disable react-hooks/exhaustive-deps */
  React.useEffect(() => {
    // draw new elements
    if (graphIsReady) {
      const elt = popElements()
      if (elt) {
        drawElements(graph, elt)
      }
    }
  }, [elementsQueue, graphIsReady, graph, popElements])
  /* eslint-enable react-hooks/exhaustive-deps */

  React.useEffect(() => {
    // store new elements in the draw queue
    if (elementsChanged) {
      addElements(elements)
    }
  }, [elements, elementsChanged, addElements])

  /* eslint-disable react-hooks/exhaustive-deps */
  React.useEffect(() => {
    if (!container.current) {
      return
    }
    if (!graph) {
      cytoscape.use(graphLayoutEngines[layoutEngine])
      const newGraph = cytoscape({
        elements: [],
        style,
        maxZoom: 1,
        wheelSensitivity: 0.2,
        container: container.current,
        selectable: false
      })
      addListeners(newGraph, handleDialogOpen)

      setGraph(newGraph)
    }
  }, [graph, container])
  /* eslint-enable react-hooks/exhaustive-deps */

  return (
    <>
      <Tooltip title="Auto Layout">
        <IconButton
          style={{ position: 'fixed', zIndex: 9999, margin: '10px' }}
          onClick={() => layout.run()}
        >
          <TransformIcon />
        </IconButton>
      </Tooltip>

      <div style={{ width: '100%', height: '100%' }} ref={container} />
      <Dialog onClose={handleDialogClose} open={open}>
        <DialogTitle>{dialogData.text}</DialogTitle>
        <DialogContent>
          <pre>{JSON.stringify(dialogData, undefined, 4)}</pre>
        </DialogContent>
      </Dialog>
    </>
  )
}

export default Diagram
