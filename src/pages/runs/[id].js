import React from 'react'
import Async from '#/components/async'
import RunDiagram from '#/components/run_diagram'
import useRunSubscription from '#/hooks/use_run_subscription'
import { useParams } from 'react-router-dom'

import Layout from '#/components/layout'


export default function Run() {
  const { id: runId } = useParams()

  return (
    <Layout>
      <Async
        Component={RunDiagram}
        hook={useRunSubscription}
        hookParams={[runId]}
      />
    </Layout>
  )
}
