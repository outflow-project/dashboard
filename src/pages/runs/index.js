import React from 'react'
import RunsTable from '#/components/runs_table'
import Async from '#/components/async'
import useRunsSubscription from '#/hooks/use_runs_subscription'
import Layout from '#/components/layout'

export default function Runs() {
  return (
    <Layout>
      <Async Component={RunsTable} hook={useRunsSubscription} />
    </Layout>
  )
}
